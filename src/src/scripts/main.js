// import Muc from 'views/muc'
// import View from 'views/view'
// import Component from 'components/tagihan.component'
// import Mitra from 'components/mitra.component'
// import Popup from 'components/popup.component'

new Vue({
    el: '#app',
    data: {
        title:"hallo guys!",
        link: 'http://www.google.com',
        htmlCode: '<b style="font-size: 24px;"> this text is using <i>v-html code</i></b>',
        counter: 0,
        x: 0,
        y: 0,
    },
    methods: {
        sayHello: function(){
            return this.title;
        },
        increase: function(){
            this.counter++;
        },
        updateCoordinate: function(e){
            this.x = e.clientX;
            this.y = e.clientY;
        },
        alertMe: function(){
            alert('alert!');
        }
    }
});

new Vue({
    el: '#app2',
    data:{
        name: 'seno',
        gantiWarna: false,
        warna: 'green',
        warna2: 'red',
        lebar: 100
    },
    computed: {
        kelas: function(){
            return {
                red : !this.gantiWarna,
                green : this.gantiWarna
            };
        },
        styleWidth: function(){
            return {
                width: this.lebar + 'px'
            }
        }
    }
});

new Vue({
    el: '#vue3',
    data: {
        fade: 'fade',
        ada: true,
        tampil: true,
        tahuIsi: ['toge, ', 'cabe, ', 'tahu'],
        osis: [
            {nama: 'seno setiaji', kelas: 'XII TKJC', umur: 14 + 'thn'}    
        ]
    }
})